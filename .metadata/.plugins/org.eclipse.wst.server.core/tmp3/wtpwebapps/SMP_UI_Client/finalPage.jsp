<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%--<link rel="stylesheet" type="text/css" href="../css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:User Home Page</title>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload="">

	<div class="header">
		<span class="titleHeader"><font color="blue">S</font><font
			color="red">.M</font>.<font color="#E3A848">P</font> </span><a
			href="SignOutServlet"><input type="submit" value="Log out"
			class="buttonLogOut" /> </a>
	</div>
	<span class="welcomeSpan loginNameFont font"> Logged in as,
		${lastName} ${firstName} </span>  <a href="homePage.jsp"><span class="home" style="margin-top:-25px;"><img src="images/home_button.png" width="70" title="Go to home!" height="70"/></span></a>
	<br />
	<br />
			<span class="font" style="margin-left:32px;color:green;" ><b><center>Congratulations, Your order has been placed. You will receive your item in an hour at your door step!</center></b>
			<br/></span><br/> <span style="margin-left:25px;color:green;"> 	<b>Your Order Details :</b></span> 
			<table border="0" style="margin-left: 25px; margin-right:30px;" cellpadding="3"
			class="adsFont">
			<tr align="center" bgcolor="yellow">
				<td width="7%">Item Id</td>
				<td width="9%">Item Name</td>
				<td width="28%" align="center">Item Description</td>
				<td width="15%">Item Price</td>
				<td width="9%">Quantities Ordered</td>
			</tr>
			<c:set var="total" value="${totalProducts}" />
			<c:set var="finalTotalProducts" value="${total-1}" />
			<c:forEach items="${beanValues}" var="key" end="${finalTotalProducts}" varStatus="id">
				<tr align="center" bgcolor="lightgreen">
					<td>${key.itemId}</td>
					<td>${key.itemName}</td>
					<td>${key.itemDescription}</td>
					<td>$${key.itemPrice}</td>
					<td>${key.quantity}</td>
				</tr>
			<input type="hidden" value="${total}" id="hiddenTotalLength"/>
			</c:forEach>
			
		</table>
			
			<form action="ViewAdsController" method="post"><br/>
				<input type="hidden" value="${username}" name="username" /> 
				<input type="submit" value="Do More Shopping!"	class="font createAdButton" />
			</form>
</body>
</html>