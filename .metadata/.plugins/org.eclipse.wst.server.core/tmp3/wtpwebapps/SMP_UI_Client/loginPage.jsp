<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%--<link rel="stylesheet" type="text/css" href="../css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>
<script type="text/javascript">
window.history.forward();
function noBack(){
	window.history.forward();
	}</script>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:Simple Market Place</title>
</head>

<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

	<div class="header">
		<span class="titleHeader"><font color="blue">S</font><font
			color="red">.M</font>.<font color="#E3A848">P</font> </span> <span
			class="signUpHeader headerSignSpan">New to S.M.P ?</span><a
			href="signUp.jsp"><input type="button" value="SIGN UP"
			class="buttonSignUp" /> </a>
	</div>
	<c:choose>
		<c:when test="${signupStatus==1}">
			<div class="signupError signupSuccess font">Successfully Registered as ${username}. Now Login! </div>
		</c:when>
	</c:choose>
	<form action="LoginController" method="post" id="loginForm">
		<div class="phoneTextSpan font">
			Welcome to the SimpleMarketPlace. Follow these simple 4 steps !<br />
			<br/>
			<center>
				1.Login with your account<br /> <br /> 2.Select the item<br /> <br />
				3.Checkout<br /> <br /> 4.Pay<br /> <br />
				<h3>Now, you can Receive your item in an hour!!</h3>
			</center>
		</div>
		<div class="phoneSpan">
			<img src="images/phone.png" alt="phone">
		</div>
		<div class="mainDiv">
			<span class="labelSignIn fontTextFields"><strong>Sign in</strong></span> <span class="logoSpan">
			</span> <strong><span class="labelUserName fontTextFields"><strong>Username</strong></span> </strong> <input
				type="text" class="textBoxSpan font" name="username" id="usrTextBox"
				autofocus="autofocus"> <strong><span
				class="labelUserName fontTextFields"><strong>Password</strong></span> </strong> <input type="password"
				class="textBoxSpan font" name="password">
			<c:choose>
				<c:when test="${status==false}">
					<div id="loginError" class="loginError fontTextFields">
						The username or password you entered is incorrect. <a
							title="Please contact admin for credentials to access the system."
							class="helpIcon">?</a>
					</div>
				</c:when>
				<c:otherwise></c:otherwise>
			</c:choose>
			<div class="signInSpan" >
				<input type="submit" value="Sign in" class="buttonSignIn fontTextFields"/>
				<input type="submit" value="Sign in" class="buttonSignIn buttonForgetPassword fontTextFields"  />
			</div>
		</div>
	</form>
</body>
</html>