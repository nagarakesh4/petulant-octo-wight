package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class signUp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\t\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<style type=\"text/css\">\r\n");
      out.write("@import url('css/loginPage.css');\r\n");
      out.write("</style>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<link rel=\"shortcut icon\" href=\"images/favicon.ico\" />\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>S.M.P:Sign Up</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<div class=\"header\">\r\n");
      out.write("\t\t<span class=\"titleHeader\"><font color=\"blue\">S</font><font\r\n");
      out.write("\t\t\tcolor=\"red\">.M</font>.<font color=\"#E3A848\">P</font> </span> <span\r\n");
      out.write("\t\t\tclass=\"signInHeader headerSignSpan\">Already have an account in\r\n");
      out.write("\t\t\tS.M.P ?</span><a href=\"loginPage.jsp\"><input type=\"button\" value=\"SIGN IN\"\r\n");
      out.write("\t\t\tclass=\"buttonSignUp\" /> </a>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\t<div class=\"signupError font\"><h2>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${signupMessage}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</h2></div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t<form action=\"SignupController\" method=\"post\">\r\n");
      out.write("\t\t<div class=\"phoneTextSpan font\">\r\n");
      out.write("\t\t\tWelcome to the SimpleMarketPlace. Follow these simple 4 steps !<br />\r\n");
      out.write("\t\t\t<br>\r\n");
      out.write("\t\t\t<center>\r\n");
      out.write("\t\t\t\t1.Login with your account<br /> <br /> 2.Select the item<br /> <br />\r\n");
      out.write("\t\t\t\t3.Checkout<br /> <br /> 4.Pay<br /> <br />\r\n");
      out.write("\t\t\t\t<h3>Now, you can Receive your item in an hour!!</h3>\r\n");
      out.write("\t\t\t</center>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"phoneSpan\">\r\n");
      out.write("\t\t\t<img src=\"images/phone.png\" alt=\"phone\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"mainDiv\">\r\n");
      out.write("\t\t\t<span class=\"labelSignIn fontTextFields\"><strong>Sign up</strong></span><span\r\n");
      out.write("\t\t\t\tclass=\"labelUserName fontTextFields\"><strong>First Name</strong></span><input type=\"text\"\r\n");
      out.write("\t\t\t\tclass=\"textBoxSpan font\" name=\"firstName\" autofocus=\"autofocus\">\r\n");
      out.write("\t\t\t\t<span class=\"labelUserName fontTextFields\"><strong>Last Name</strong></span><input\r\n");
      out.write("\t\t\t\ttype=\"text\" class=\"textBoxSpan font\" name=\"lastName\"> <span\r\n");
      out.write("\t\t\t\tclass=\"labelUserName fontTextFields\"><strong>Email</strong></span><input type=\"text\"\r\n");
      out.write("\t\t\t\tclass=\"textBoxSpan font\" name=\"emailid\"> <span\r\n");
      out.write("\t\t\t\tclass=\"labelUserName fontTextFields\"><strong>Password</strong></span><input type=\"password\"\r\n");
      out.write("\t\t\t\tclass=\"textBoxSpan font\" name=\"password\">\r\n");
      out.write("\t\t\t<div class=\"signInSpan\">\r\n");
      out.write("\t\t\t\t<input type=\"submit\" value=\"Sign up\" class=\"buttonSignIn fontTextFields\" />\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</form>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
