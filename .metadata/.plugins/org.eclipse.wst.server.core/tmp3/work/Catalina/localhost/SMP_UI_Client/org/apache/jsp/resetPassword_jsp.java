package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class resetPassword_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE HTML> \r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<style>\r\n");
      out.write(".error {color: #FF0000;}\r\n");
      out.write("</style>\r\n");
      out.write("</head>\r\n");
      out.write("<body> \r\n");
      out.write("\r\n");
      out.write("<?php\r\n");
      out.write("// define variables and set to empty values\r\n");
      out.write("$nameErr = $emailErr = $genderErr = $websiteErr = \"\";\r\n");
      out.write("$name = $email = $gender = $comment = $website = \"\";\r\n");
      out.write("\r\n");
      out.write("if ($_SERVER[\"REQUEST_METHOD\"] == \"POST\")\r\n");
      out.write("{\r\n");
      out.write("   if (empty($_POST[\"name\"]))\r\n");
      out.write("     {$nameErr = \"Name is required\";}\r\n");
      out.write("   else\r\n");
      out.write("     {\r\n");
      out.write("     $name = test_input($_POST[\"name\"]);\r\n");
      out.write("     // check if name only contains letters and whitespace\r\n");
      out.write("     if (!preg_match(\"/^[a-zA-Z ]*$/\",$name))\r\n");
      out.write("       {\r\n");
      out.write("       $nameErr = \"Only letters and white space allowed\"; \r\n");
      out.write("       }\r\n");
      out.write("     }\r\n");
      out.write("   \r\n");
      out.write("   if (empty($_POST[\"email\"]))\r\n");
      out.write("     {$emailErr = \"Email is required\";}\r\n");
      out.write("   else\r\n");
      out.write("     {\r\n");
      out.write("     $email = test_input($_POST[\"email\"]);\r\n");
      out.write("     // check if e-mail address syntax is valid\r\n");
      out.write("     if (!preg_match(\"/([\\w\\-]+\\@[\\w\\-]+\\.[\\w\\-]+)/\",$email))\r\n");
      out.write("       {\r\n");
      out.write("       $emailErr = \"Invalid email format\"; \r\n");
      out.write("       }\r\n");
      out.write("     }\r\n");
      out.write("     \r\n");
      out.write("   if (empty($_POST[\"website\"]))\r\n");
      out.write("     {$website = \"\";}\r\n");
      out.write("   else\r\n");
      out.write("     {\r\n");
      out.write("     $website = test_input($_POST[\"website\"]);\r\n");
      out.write("     // check if URL address syntax is valid (this regular expression also allows dashes in the URL)\r\n");
      out.write("     if (!preg_match(\"/\\b(?:(?:https?|ftp):\\/\\/|www\\.)[-a-z0-9+&@#\\/%?=~_|!:,.;]*[-a-z0-9+&@#\\/%=~_|]/i\",$website))\r\n");
      out.write("       {\r\n");
      out.write("       $websiteErr = \"Invalid URL\"; \r\n");
      out.write("       }\r\n");
      out.write("     }\r\n");
      out.write("\r\n");
      out.write("   if (empty($_POST[\"comment\"]))\r\n");
      out.write("     {$comment = \"\";}\r\n");
      out.write("   else\r\n");
      out.write("     {$comment = test_input($_POST[\"comment\"]);}\r\n");
      out.write("\r\n");
      out.write("   if (empty($_POST[\"gender\"]))\r\n");
      out.write("     {$genderErr = \"Gender is required\";}\r\n");
      out.write("   else\r\n");
      out.write("     {$gender = test_input($_POST[\"gender\"]);}\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("function test_input($data)\r\n");
      out.write("{\r\n");
      out.write("     $data = trim($data);\r\n");
      out.write("     $data = stripslashes($data);\r\n");
      out.write("     $data = htmlspecialchars($data);\r\n");
      out.write("     return $data;\r\n");
      out.write("}\r\n");
      out.write("?>\r\n");
      out.write("\r\n");
      out.write("<h2>PHP Form Validation Example</h2>\r\n");
      out.write("<p><span class=\"error\">* required field.</span></p>\r\n");
      out.write("<form method=\"post\" action=\"<?php echo htmlspecialchars($_SERVER[\"PHP_SELF\"]);?>\"> \r\n");
      out.write("   Name: <input type=\"text\" name=\"name\">\r\n");
      out.write("   <span class=\"error\">* <?php echo $nameErr;?></span>\r\n");
      out.write("   <br><br>\r\n");
      out.write("   E-mail: <input type=\"text\" name=\"email\">\r\n");
      out.write("   <span class=\"error\">* <?php echo $emailErr;?></span>\r\n");
      out.write("   <br><br>\r\n");
      out.write("   Website: <input type=\"text\" name=\"website\">\r\n");
      out.write("   <span class=\"error\"><?php echo $websiteErr;?></span>\r\n");
      out.write("   <br><br>\r\n");
      out.write("   Comment: <textarea name=\"comment\" rows=\"5\" cols=\"40\"></textarea>\r\n");
      out.write("   <br><br>\r\n");
      out.write("   Gender:\r\n");
      out.write("   <input type=\"radio\" name=\"gender\" value=\"female\">Female\r\n");
      out.write("   <input type=\"radio\" name=\"gender\" value=\"male\">Male\r\n");
      out.write("   <span class=\"error\">* <?php echo $genderErr;?></span>\r\n");
      out.write("   <br><br>\r\n");
      out.write("   <input type=\"submit\" name=\"submit\" value=\"Submit\"> \r\n");
      out.write("</form>\r\n");
      out.write("\r\n");
      out.write("<?php\r\n");
      out.write("echo \"<h2>Your Input:</h2>\";\r\n");
      out.write("echo $name;\r\n");
      out.write("echo \"<br>\";\r\n");
      out.write("echo $email;\r\n");
      out.write("echo \"<br>\";\r\n");
      out.write("echo $website;\r\n");
      out.write("echo \"<br>\";\r\n");
      out.write("echo $comment;\r\n");
      out.write("echo \"<br>\";\r\n");
      out.write("echo $gender;\r\n");
      out.write("?>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
