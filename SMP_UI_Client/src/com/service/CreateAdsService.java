/**
 * CreateAdsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public interface CreateAdsService extends java.rmi.Remote {
    public java.lang.String[] createAds(java.lang.String itemName, java.lang.String itemDescription, java.lang.String sellerInformation, java.lang.String itemPrice, java.lang.String quantity, java.lang.String itemId, java.lang.String username) throws java.rmi.RemoteException;
}
