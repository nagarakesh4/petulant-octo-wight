/**
 * RemoveFromCartService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public interface RemoveFromCartService extends java.rmi.Remote {
    public int[] removeFromCart(com.beans.AdsBean adsBean) throws java.rmi.RemoteException;
}
