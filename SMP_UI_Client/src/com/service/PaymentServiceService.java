/**
 * PaymentServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public interface PaymentServiceService extends javax.xml.rpc.Service {
    public java.lang.String getPaymentServiceAddress();

    public com.service.PaymentService getPaymentService() throws javax.xml.rpc.ServiceException;

    public com.service.PaymentService getPaymentService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
