/**
 * RemoveFromCartServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public class RemoveFromCartServiceServiceLocator extends org.apache.axis.client.Service implements com.service.RemoveFromCartServiceService {

    public RemoveFromCartServiceServiceLocator() {
    }


    public RemoveFromCartServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RemoveFromCartServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RemoveFromCartService
    private java.lang.String RemoveFromCartService_address = "http://localhost:8080/SMP_UI_/services/RemoveFromCartService";

    public java.lang.String getRemoveFromCartServiceAddress() {
        return RemoveFromCartService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RemoveFromCartServiceWSDDServiceName = "RemoveFromCartService";

    public java.lang.String getRemoveFromCartServiceWSDDServiceName() {
        return RemoveFromCartServiceWSDDServiceName;
    }

    public void setRemoveFromCartServiceWSDDServiceName(java.lang.String name) {
        RemoveFromCartServiceWSDDServiceName = name;
    }

    public com.service.RemoveFromCartService getRemoveFromCartService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RemoveFromCartService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRemoveFromCartService(endpoint);
    }

    public com.service.RemoveFromCartService getRemoveFromCartService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.service.RemoveFromCartServiceSoapBindingStub _stub = new com.service.RemoveFromCartServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getRemoveFromCartServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRemoveFromCartServiceEndpointAddress(java.lang.String address) {
        RemoveFromCartService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.service.RemoveFromCartService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.service.RemoveFromCartServiceSoapBindingStub _stub = new com.service.RemoveFromCartServiceSoapBindingStub(new java.net.URL(RemoveFromCartService_address), this);
                _stub.setPortName(getRemoveFromCartServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RemoveFromCartService".equals(inputPortName)) {
            return getRemoveFromCartService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.com", "RemoveFromCartServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.com", "RemoveFromCartService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RemoveFromCartService".equals(portName)) {
            setRemoveFromCartServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
