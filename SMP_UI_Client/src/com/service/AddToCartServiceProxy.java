package com.service;

public class AddToCartServiceProxy implements com.service.AddToCartService {
  private String _endpoint = null;
  private com.service.AddToCartService addToCartService = null;
  
  public AddToCartServiceProxy() {
    _initAddToCartServiceProxy();
  }
  
  public AddToCartServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initAddToCartServiceProxy();
  }
  
  private void _initAddToCartServiceProxy() {
    try {
      addToCartService = (new com.service.AddToCartServiceServiceLocator()).getAddToCartService();
      if (addToCartService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)addToCartService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)addToCartService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (addToCartService != null)
      ((javax.xml.rpc.Stub)addToCartService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.AddToCartService getAddToCartService() {
    if (addToCartService == null)
      _initAddToCartServiceProxy();
    return addToCartService;
  }
  
  public int[] addToCart(com.beans.AdsBean adsBean) throws java.rmi.RemoteException{
    if (addToCartService == null)
      _initAddToCartServiceProxy();
    return addToCartService.addToCart(adsBean);
  }
  
  
}