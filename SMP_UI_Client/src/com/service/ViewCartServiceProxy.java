package com.service;

public class ViewCartServiceProxy implements com.service.ViewCartService {
  private String _endpoint = null;
  private com.service.ViewCartService viewCartService = null;
  
  public ViewCartServiceProxy() {
    _initViewCartServiceProxy();
  }
  
  public ViewCartServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initViewCartServiceProxy();
  }
  
  private void _initViewCartServiceProxy() {
    try {
      viewCartService = (new com.service.ViewCartServiceServiceLocator()).getViewCartService();
      if (viewCartService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)viewCartService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)viewCartService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (viewCartService != null)
      ((javax.xml.rpc.Stub)viewCartService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.ViewCartService getViewCartService() {
    if (viewCartService == null)
      _initViewCartServiceProxy();
    return viewCartService;
  }
  
  public com.beans.AdsBean[] retrieveCart(java.lang.String username) throws java.rmi.RemoteException{
    if (viewCartService == null)
      _initViewCartServiceProxy();
    return viewCartService.retrieveCart(username);
  }
  
  
}