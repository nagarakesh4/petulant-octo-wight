/**
 * PaymentService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public interface PaymentService extends java.rmi.Remote {
    public int checkOut(java.lang.String buyerUserName, java.lang.String ccnumber) throws java.rmi.RemoteException;
}
