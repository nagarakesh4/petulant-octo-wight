package com.service;

public class PaymentServiceProxy implements com.service.PaymentService {
  private String _endpoint = null;
  private com.service.PaymentService paymentService = null;
  
  public PaymentServiceProxy() {
    _initPaymentServiceProxy();
  }
  
  public PaymentServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initPaymentServiceProxy();
  }
  
  private void _initPaymentServiceProxy() {
    try {
      paymentService = (new com.service.PaymentServiceServiceLocator()).getPaymentService();
      if (paymentService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)paymentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)paymentService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (paymentService != null)
      ((javax.xml.rpc.Stub)paymentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.PaymentService getPaymentService() {
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService;
  }
  
  public int checkOut(java.lang.String buyerUserName, java.lang.String ccnumber) throws java.rmi.RemoteException{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.checkOut(buyerUserName, ccnumber);
  }
  
  
}