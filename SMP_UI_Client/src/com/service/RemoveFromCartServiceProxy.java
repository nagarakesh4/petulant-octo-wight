package com.service;

public class RemoveFromCartServiceProxy implements com.service.RemoveFromCartService {
  private String _endpoint = null;
  private com.service.RemoveFromCartService removeFromCartService = null;
  
  public RemoveFromCartServiceProxy() {
    _initRemoveFromCartServiceProxy();
  }
  
  public RemoveFromCartServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initRemoveFromCartServiceProxy();
  }
  
  private void _initRemoveFromCartServiceProxy() {
    try {
      removeFromCartService = (new com.service.RemoveFromCartServiceServiceLocator()).getRemoveFromCartService();
      if (removeFromCartService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)removeFromCartService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)removeFromCartService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (removeFromCartService != null)
      ((javax.xml.rpc.Stub)removeFromCartService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.RemoveFromCartService getRemoveFromCartService() {
    if (removeFromCartService == null)
      _initRemoveFromCartServiceProxy();
    return removeFromCartService;
  }
  
  public int[] removeFromCart(com.beans.AdsBean adsBean) throws java.rmi.RemoteException{
    if (removeFromCartService == null)
      _initRemoveFromCartServiceProxy();
    return removeFromCartService.removeFromCart(adsBean);
  }
  
  
}