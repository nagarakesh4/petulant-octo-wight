package com.Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beans.AdsBean;
import com.service.RemoveFromCartServiceProxy;
import com.service.ViewCartServiceProxy;

/**
 * Servlet implementation class RemoveFromCartController
 */
public class RemoveFromCartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveFromCartController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		
		System.out.println("in remove cart controller");
		String id = request.getParameter("itemId");
		String buname = request.getParameter("buyerUserName");
		System.out.println("controller id "+id);
		System.out.println("controller name "+buname);
		
		AdsBean adsBean = new AdsBean();
		adsBean.setItemId(id);
		adsBean.setUserName(buname);
		
		
		RemoveFromCartServiceProxy removeFromCartServiceProxy = new RemoveFromCartServiceProxy();
		removeFromCartServiceProxy.setEndpoint("http://localhost:8080/SMP_UI_/services/RemoveFromCartService");
		removeFromCartServiceProxy.removeFromCart(adsBean);
		
		ViewCartServiceProxy viewCartServiceProxy = new ViewCartServiceProxy();
		viewCartServiceProxy.setEndpoint("http://localhost:8080/SMP_UI_/services/ViewCartService");
		
		
		
		AdsBean[] results = viewCartServiceProxy.retrieveCart(buname);
		int index = 0;
		while (results[index] != null) {
			index++;
		}
		float totalValue = results[index-1].getTotalValueOfItems();
		int totalProductsInCart = results[index-1].getTotalItemsInCart();
		String nextJSP = "viewCart.jsp";
		
		
		session.setAttribute("totalValueOfItems", totalValue);
		session.setAttribute("totalProductsInCart", totalProductsInCart);
		session.setAttribute("totalProducts", index-1);
		session.setAttribute("beanValues", results);
		response.sendRedirect(nextJSP);
		
	}

}
