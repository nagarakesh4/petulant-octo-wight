package com.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.service.LoginServiceProxy;

/**
 * Servlet implementation class loginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		LoginServiceProxy lsp = new LoginServiceProxy();
		lsp.setEndpoint("http://localhost:8080/SMP_UI_/services/LoginService");
		String[] results = lsp.checkDetails(username, password);
		if (results[0].equals("true")) {
			String nextJSP = "/homePage.jsp";
			redirectToNextPage(nextJSP, username, request, session, response, results);
		} else {
			String nextJSP = "/loginPage.jsp";
			redirectToNextPage(nextJSP, username, request, session, response, results);
		}
	}

	private void redirectToNextPage(String nextJSP, String username,HttpServletRequest request,
			HttpSession session, HttpServletResponse response, String[] results)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		session.setAttribute("username", username);
		request.setAttribute("status", results[0]);
		session.setAttribute("lastAccessTime", results[1]);
		session.setAttribute("countLoginTime", results[2]);
		session.setAttribute("totalProductsInCart", results[3]);
		session.setAttribute("firstName", results[4]);
		session.setAttribute("lastName", results[5]);
		
		dispatcher.forward(request,response);
	}

}
