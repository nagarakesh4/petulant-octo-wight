package com.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beans.AdsBean;
import com.service.AddToCartServiceProxy;
import com.service.ViewAdsServiceProxy;

/**
 * Servlet implementation class CartAddController
 */
public class CartAddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CartAddController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		System.out.println("in add cart controller");
		String id = request.getParameter("itemId");
		String iname = request.getParameter("itemName");
		String idescr = request.getParameter("itemDescription");
		String iprice = request.getParameter("itemPrice");
		String iquant = request.getParameter("quantity");
		String isi = request.getParameter("sellerInformation");
		String buname = request.getParameter("buyerUserName");
		String suname = request.getParameter("sellerUserName");	
		
		System.out.println("you have selected item id " + id);
		System.out.println("you have selected item name " + iname);
		System.out.println("you have selected item description " + idescr);
		System.out.println("you have selected item price " + iprice);
		System.out.println("you have selected item quantity " + iquant);
		System.out.println("you have selected item seller info " + isi);
		System.out.println("brought by " + buname);
		System.out.println("sold by " + suname);
		
		AdsBean adsBean = new AdsBean();
		adsBean.setItemId(id);
		adsBean.setItemName(iname);
		adsBean.setItemDescription(idescr);
		adsBean.setItemPrice(Float.parseFloat(iprice));
		adsBean.setQuantity(Integer.parseInt(iquant));
		adsBean.setSellerInformation(isi);
		adsBean.setUserName(buname);
		adsBean.setSellerUserName(suname);
		
		AddToCartServiceProxy addToCartServiceProxy = new AddToCartServiceProxy();
		addToCartServiceProxy
				.setEndpoint("http://localhost:8080/SMP_UI_/services/AddToCartService");
		int resultsFromCart[] = addToCartServiceProxy.addToCart(adsBean);
		
		ViewAdsServiceProxy viewAdsServiceProxy = new ViewAdsServiceProxy();
		viewAdsServiceProxy
				.setEndpoint("http://localhost:8080/SMP_UI_/services/ViewAdsService");

		AdsBean[] results = viewAdsServiceProxy.retrieveAds(buname);
		int index = 0;
		while (results[index] != null) {
			index++;
		}
		System.out.println("success or failure "+resultsFromCart[0]);
		String nextJSP = "/viewAdvertisements.jsp";
		System.out.println(resultsFromCart[1] + " (from controller) in cart are present");
		System.out.println("statusAddCart "+resultsFromCart[0] );
		RequestDispatcher dispatcher = getServletContext()
		.getRequestDispatcher(nextJSP);
		session.setAttribute("totalProductsInCart", resultsFromCart[1]);
		request.setAttribute("addCartStatus", resultsFromCart[0]);
		session.setAttribute("totalProducts", index);
		session.setAttribute("beanValues", results);
		dispatcher.forward(request, response);
	}
}
