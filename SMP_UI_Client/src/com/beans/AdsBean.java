/**
 * AdsBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.beans;

public class AdsBean  implements java.io.Serializable {
    private java.lang.String itemDescription;

    private java.lang.String itemId;

    private java.lang.String itemName;

    private float itemPrice;

    private int quantity;

    private java.lang.String sellerInformation;

    private java.lang.String sellerUserName;

    private int totalItemsInCart;

    private float totalValueOfItems;

    private java.lang.String userName;

    public AdsBean() {
    }

    public AdsBean(
           java.lang.String itemDescription,
           java.lang.String itemId,
           java.lang.String itemName,
           float itemPrice,
           int quantity,
           java.lang.String sellerInformation,
           java.lang.String sellerUserName,
           int totalItemsInCart,
           float totalValueOfItems,
           java.lang.String userName) {
           this.itemDescription = itemDescription;
           this.itemId = itemId;
           this.itemName = itemName;
           this.itemPrice = itemPrice;
           this.quantity = quantity;
           this.sellerInformation = sellerInformation;
           this.sellerUserName = sellerUserName;
           this.totalItemsInCart = totalItemsInCart;
           this.totalValueOfItems = totalValueOfItems;
           this.userName = userName;
    }


    /**
     * Gets the itemDescription value for this AdsBean.
     * 
     * @return itemDescription
     */
    public java.lang.String getItemDescription() {
        return itemDescription;
    }


    /**
     * Sets the itemDescription value for this AdsBean.
     * 
     * @param itemDescription
     */
    public void setItemDescription(java.lang.String itemDescription) {
        this.itemDescription = itemDescription;
    }


    /**
     * Gets the itemId value for this AdsBean.
     * 
     * @return itemId
     */
    public java.lang.String getItemId() {
        return itemId;
    }


    /**
     * Sets the itemId value for this AdsBean.
     * 
     * @param itemId
     */
    public void setItemId(java.lang.String itemId) {
        this.itemId = itemId;
    }


    /**
     * Gets the itemName value for this AdsBean.
     * 
     * @return itemName
     */
    public java.lang.String getItemName() {
        return itemName;
    }


    /**
     * Sets the itemName value for this AdsBean.
     * 
     * @param itemName
     */
    public void setItemName(java.lang.String itemName) {
        this.itemName = itemName;
    }


    /**
     * Gets the itemPrice value for this AdsBean.
     * 
     * @return itemPrice
     */
    public float getItemPrice() {
        return itemPrice;
    }


    /**
     * Sets the itemPrice value for this AdsBean.
     * 
     * @param itemPrice
     */
    public void setItemPrice(float itemPrice) {
        this.itemPrice = itemPrice;
    }


    /**
     * Gets the quantity value for this AdsBean.
     * 
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this AdsBean.
     * 
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the sellerInformation value for this AdsBean.
     * 
     * @return sellerInformation
     */
    public java.lang.String getSellerInformation() {
        return sellerInformation;
    }


    /**
     * Sets the sellerInformation value for this AdsBean.
     * 
     * @param sellerInformation
     */
    public void setSellerInformation(java.lang.String sellerInformation) {
        this.sellerInformation = sellerInformation;
    }


    /**
     * Gets the sellerUserName value for this AdsBean.
     * 
     * @return sellerUserName
     */
    public java.lang.String getSellerUserName() {
        return sellerUserName;
    }


    /**
     * Sets the sellerUserName value for this AdsBean.
     * 
     * @param sellerUserName
     */
    public void setSellerUserName(java.lang.String sellerUserName) {
        this.sellerUserName = sellerUserName;
    }


    /**
     * Gets the totalItemsInCart value for this AdsBean.
     * 
     * @return totalItemsInCart
     */
    public int getTotalItemsInCart() {
        return totalItemsInCart;
    }


    /**
     * Sets the totalItemsInCart value for this AdsBean.
     * 
     * @param totalItemsInCart
     */
    public void setTotalItemsInCart(int totalItemsInCart) {
        this.totalItemsInCart = totalItemsInCart;
    }


    /**
     * Gets the totalValueOfItems value for this AdsBean.
     * 
     * @return totalValueOfItems
     */
    public float getTotalValueOfItems() {
        return totalValueOfItems;
    }


    /**
     * Sets the totalValueOfItems value for this AdsBean.
     * 
     * @param totalValueOfItems
     */
    public void setTotalValueOfItems(float totalValueOfItems) {
        this.totalValueOfItems = totalValueOfItems;
    }


    /**
     * Gets the userName value for this AdsBean.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this AdsBean.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdsBean)) return false;
        AdsBean other = (AdsBean) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.itemDescription==null && other.getItemDescription()==null) || 
             (this.itemDescription!=null &&
              this.itemDescription.equals(other.getItemDescription()))) &&
            ((this.itemId==null && other.getItemId()==null) || 
             (this.itemId!=null &&
              this.itemId.equals(other.getItemId()))) &&
            ((this.itemName==null && other.getItemName()==null) || 
             (this.itemName!=null &&
              this.itemName.equals(other.getItemName()))) &&
            this.itemPrice == other.getItemPrice() &&
            this.quantity == other.getQuantity() &&
            ((this.sellerInformation==null && other.getSellerInformation()==null) || 
             (this.sellerInformation!=null &&
              this.sellerInformation.equals(other.getSellerInformation()))) &&
            ((this.sellerUserName==null && other.getSellerUserName()==null) || 
             (this.sellerUserName!=null &&
              this.sellerUserName.equals(other.getSellerUserName()))) &&
            this.totalItemsInCart == other.getTotalItemsInCart() &&
            this.totalValueOfItems == other.getTotalValueOfItems() &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItemDescription() != null) {
            _hashCode += getItemDescription().hashCode();
        }
        if (getItemId() != null) {
            _hashCode += getItemId().hashCode();
        }
        if (getItemName() != null) {
            _hashCode += getItemName().hashCode();
        }
        _hashCode += new Float(getItemPrice()).hashCode();
        _hashCode += getQuantity();
        if (getSellerInformation() != null) {
            _hashCode += getSellerInformation().hashCode();
        }
        if (getSellerUserName() != null) {
            _hashCode += getSellerUserName().hashCode();
        }
        _hashCode += getTotalItemsInCart();
        _hashCode += new Float(getTotalValueOfItems()).hashCode();
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdsBean.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.com", "AdsBean"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "itemDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "itemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "itemName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "itemPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellerInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "sellerInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellerUserName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "sellerUserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalItemsInCart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "totalItemsInCart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalValueOfItems");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "totalValueOfItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.com", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
