<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page errorPage="ErrorPage.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%--<link rel="stylesheet" type="text/css" href="css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>

<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:User Home Page</title>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload="">

	<form method="get" action="SignOutServlet">
		<div class="header">
			<span class="titleHeader"><font color="blue">S</font><font
				color="red">.M</font>.<font color="#E3A848">P</font> </span> <a
				href="loginPage.jsp"><input type="button" value="Log out"
				class="buttonLogOut"/> </a>
		</div>
	</form>
	<c:choose>
		<c:when test="${createAdStatus==1}">
			<div class="signupError adsSuccess font"><h2>Advertisement
						created successfully with Id : ${itemId}</h2></div>
		</c:when>
	</c:choose>			
	<span class="lastAccessTimeSpan font"> <c:choose>
			<c:when test="${countLoginTime==1}">
				<font color="yellow">First Time Logged-In!
				</font>
			</c:when>
			<c:otherwise>
				<font color="black">Your Last Logged-In time</font>
				<br />
				<br />
				<span class="time">${lastAccessTime}</span>
			</c:otherwise>
		</c:choose> 
		
		</span>

	<span class="welcomeSpan loginNameFont font">Welcome,
		${lastName} ${firstName}!</span>
	<br />
	<br />
	<table cellpadding="0" class="advertisementsOptionsSpan font">
		<c:if test="${totalProductsInCart>0}"><tr>
			<td class="textFont" align="right">View & edit your cart here</td>
			<td class="textFont"><span class="loginNameFont"><a href="DisplayCartController?buyerUserName=${username}">
		 	 <input type="image" src="images/filled_cart.jpg" title="Watch your cart" alt="Submit" height="70" width="70" />
	    </a>${totalProductsInCart} items </span>
			</td>
		</tr></c:if>
		<tr>

			<td class="textFont">View Advertisements and buy products</td>
			<td class="textFont">
				<form action="ViewAdsController" method="post">
					<input type="hidden" value="${username}" name="username" /> <input
						type="submit" value="View Advertisement"
						class="font createAdButton" />
				</form></td>
		</tr>
		<tr>
			<td class="textFont">Create a new advertisement and publish</td>
			<td class="textFont"><a href="createAdvertisement.jsp"><input
					type="button" value="Create Advertisement"
					class="font createAdButton" /> </a></td>
		</tr>
		<tr>

			<td class="textFont">View Your History</td>
			<td class="textFont">
				<form action="TransactionsController" method="post">
					<input type="hidden" value="${username}" name="username" /> <input
						type="submit" value="Check History"
						class="font createAdButton" />
				</form></td>
		</tr>
		<tr>

			<td class="textFont">View New Feature</td>
			<td class="textFont">
				<form action="DuckDuckGoServlet" method="post">
					<input type="hidden" value="${username}" name="username" /> <input
						type="submit" value="New Feature"
						class="font createAdButton" />
				</form></td>
		</tr>
	</table>



</body>
</html>