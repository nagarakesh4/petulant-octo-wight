<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html ng-app="demoApp">
<head>
	<script src="js/angular.min.js"></script>
<%--<link rel="stylesheet" type="text/css" href="../css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
	function goTop(){
		window.scrollTo(0,5);		
	}
	var demoApp = angular.module('demoApp',[]);
	
	
	
	demoApp.controller('SimpleController', function ($scope){
		$scope.countries=[
			{name:'Sri Lanka',capital:'Colombo'},
			{name:'Australia',capital:'Melbourne'},
			{name:'Singapore',capital:'Singapore'},
			{name:'India',capital:'Delhi'},
			{name:'India',capital:'Mumbai'},
			{name:'India',capital:'Kolkata'},
			{name:'India',capital:'Chennai'},
			{name:'India',capital:'Hyderabad'}
		];
	});
</script>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:User Home Page</title>
</head>

<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

	<div class="header">
		<span class="titleHeader"><font color="blue">S</font><font
			color="red">.M</font>.<font color="#E3A848">P</font> </span><a
			href="SignOutServlet"><input type="submit" value="Log out"
			class="buttonLogOut" /> </a>
	</div>
	<span class="welcomeSpan loginNameFont font"> Logged in as,
		${lastName} ${firstName} <c:if test="${totalProductsInCart>0}">
			<span class="editIcon"> <img src="images/edit_cart.gif"
				width="290" height="40"><img src="images/arrow.png" /> </span>
		</c:if> <span class="cartIcon fontCart">
		
		<c:if test="${totalProductsInCart>0}"><input type="hidden" value="${username}" name="buyerUserName" />
		<a href="DisplayCartController?buyerUserName=${username}">
		 	 <input type="image" src="images/filled_cart.jpg" title="Watch your cart" alt="Submit" height="70" width="70" />
	    </a>${totalProductsInCart} item(s) in your cart</c:if>
				<c:if test="${totalProductsInCart==0 || totalProductsInCart==null }"><input type="hidden" value="${username}" name="buyerUserName" />
		<a href="DisplayCartController?buyerUserName=${username}">
		 	 <input type="image" src="images/empty-cart.jpg" title="Watch your cart" alt="Submit" height="70" width="70" />
	    </a>no items in your cart</c:if>
	    <a href="homePage.jsp"><span class="home" style="margin-top:-10px;margin-right:20px;"><img src="images/home_button.png" width="70" title="Go to home!" height="70"/></span></a>
	</span> </span>
	<br /><br/>
	<!-- <span ng-controller="SimpleController">
		Name:
		<br/>
		<input type="text" data-ng-model="beanValues" autofocus/> {{name}}
		<br/>
		<span data-ng-repeat="country in countries | filter:name | orderBy:'capital'">{{country.name | uppercase}} - {{country.capital | lowercase}}</span>
	</span> -->
	<hr/>
	
	<c:if test="${addCartStatus==0}"><span class="error"><font color="red">One or more same Items are in cart, updated the quantities.<br/></font></span></c:if><br/>
	<c:if test="${totalProducts>0}">
		<span class="productsFont">Showing 1-${totalProducts} of
			Results </span>
		<br />
		<br />
		
		<c:set var="total" value="${totalProducts}" />
		<c:set var="finalTotalProducts" value="${total-1}" />
		<c:forEach items="${beanValues}" var="key" end="${finalTotalProducts}">
			<table border="10" style="margin-left: 25px;" cellpadding="5"
				class="font">
				<colgroup>
					<col style="width: 450px" />
					<col style="width: 550px" />
					<col style="width: 250px" />
				</colgroup>
				<tr class="tableAgainHeader">
					<td><b>Items Id</b>
					</td>
					<td class="dataAgain"><b>${key.itemId}</b>
					</td>
					<td rowspan="6" bgcolor="lightgrey">
						<form action="CartAddController" method="post">
							<input type="hidden" value="${key.itemId}" name="itemId" /> <input
								type="hidden" value="${key.itemName}" name="itemName" /> <input
								type="hidden" value="${key.itemDescription}"
								name="itemDescription" /> <input type="hidden"
								value="${key.itemPrice}" name="itemPrice" /> <input
								type="hidden" value="${key.quantity}" name="quantity" /> <input
								type="hidden" value="${key.sellerInformation}"
								name="sellerInformation" /> <input type="hidden"
								value="${username}" name="buyerUserName" />
								<input type="hidden"
								value="${key.userName}" name="sellerUserName" />
							<!-- <input type="submit" value="Add To Cart" /> -->
							<input type="image" src="images/add_to_cart.png" title="Add to Cart" alt="Submit"
								height="100" width="100" />
						</form>
					</td>

				</tr>
				<tr class="tableAgainHeader">
					<td><b>Item Name</b>
					</td>
					<td class="titleName"><b>${key.itemName}</b></td>
				</tr>
				<tr class="tableAgainHeader">
					<td><b>Description</b>
					</td>
					<td class="dataAgain">${key.itemDescription}</td>
				</tr>
				<tr class="tableAgainHeader">
					<td><b>Price</b>
					</td>
					<td class="dataAgain"><b>$${key.itemPrice}</b></td>
				</tr>
				<tr class="tableAgainHeader">
					<td><b>Quantities Available</b>
					</td>
					<td class="dataAgain">${key.quantity} piece(s)</td>
				</tr>
				<tr class="tableAgainHeader">
					<td><b>Seller Information</b>
					</td>
					<td class="dataAgain">${key.sellerInformation}</td>
				</tr>
				
			</table>
			<br/><br/>
			<input type="button" value="go Top!" style="margin-top:-38px;" class="goTopClass font goTopButton" onclick="goTop()"/>
		</c:forEach>
	
	</c:if>
	<c:if test="${totalProducts<1}">
		<span class="font error">Sorry, there are no items to display.
			<c:if test="${totalProductsInCart>0}">You have items in the cart to checkout!</c:if>
		</span>
	</c:if>
</body>
</html>