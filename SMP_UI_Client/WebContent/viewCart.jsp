<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%--<link rel="stylesheet" type="text/css" href="../css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
	function calculateTotal(){
		var totalResults = document.getElementById("hiddenTotalLength").value;
		totalResultss = parseInt(totalResults);
		var totalValue = 0;
		for(var i=0;i<totalResultss;i++){
			var price = document.getElementById("price"+i).innerHTML;
			price = price.split("$");
			price = price[1];
			price = parseFloat(price);
			var quantity = document.getElementById("quantity"+i).innerHTML;
			quantity = parseFloat(quantity);
			totalValue =totalValue+ price*quantity;
		}
		document.getElementById("totalAmount").value = "$"+totalValue.toFixed(2); 
		document.getElementById("totalAmountHidden").value = "$"+totalValue.toFixed(2); 
	}
	function submitForm(){
		window.location.href = "CheckoutController?buyerUserName="+document.getElementById("hiddenUserName").value+"&grandTotal="+Math.random()+document.getElementById("totalAmountHidden").value+";"+Math.random()+Math.random();
	}
</script>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:User Home Page</title>
</head>

<body onload="noBack();calculateTotal()" onpageshow="if (event.persisted) noBack();" onunload="" >

	<div class="header">
		<span class="titleHeader"><font color="blue">S</font><font
			color="red">.M</font>.<font color="#E3A848">P</font> </span><a
			href="SignOutServlet"><input type="submit" value="Log out"
			class="buttonLogOut" /> </a>
	</div>
	<span class="welcomeSpan loginNameFont font"> Logged in as,
		${lastName} ${firstName}  <span class="cartIcon fontCart">
		
	  <a href="homePage.jsp"><span class="home"><img src="images/home_button.png" width="70" title="Go to home!" height="70"/></span></a>
	</span> </span>
	<br /><c:if test="${totalProducts>0}">
		<span class="productsFont">Your Cart,
			Check out now!</span>
		<br />
		<br />
		<table border="0" style="margin-left: 25px; margin-right:30px;" cellpadding="3"
			class="adsFont">
			<tr align="center" bgcolor="yellow">
				<td width="7%">Item Id</td>
				<td width="9%">Item Name</td>
				<td width="28%" align="center">Item Description</td>
				<td width="15%">Seller Information</td>
				<td width="15%">Item Price</td>
				<td width="9%">Quantities Ordered</td>
				<td>Remove from Cart</td>
			</tr>
			<c:set var="total" value="${totalProducts}" />
			<c:set var="finalTotalProducts" value="${total-1}" />
			<c:forEach items="${beanValues}" var="key"
				end="${finalTotalProducts}" varStatus="id">
				<tr align="center" bgcolor="lightgreen">
					<td>${key.itemId}</td>
					<td>${key.itemName}</td>
					<td>${key.itemDescription}</td>
					<td>${key.sellerInformation}</td>
					<td id="price${id.index}">$${key.itemPrice}</td>
					<td id="quantity${id.index}">${key.quantity}</td>


					<td>
						<form action="RemoveFromCartController" method="post">
							<input type="hidden" value="${key.itemId}" name="itemId" /> <input type="hidden"
								value="${username}" name="buyerUserName" />
							<!-- <input type="submit" value="Add To Cart" /> -->
							<input type="image" src="images/remove_from_cart.png"
								title="Remove from cart" alt="Submit" height="70"
								width="70" />
						</form></td>
				</tr>
			<input type="hidden" value="${total}" id="hiddenTotalLength"/>
			</c:forEach>
			<tr>
				<td></td>
				<td></td>
				<td align="center" colspan="2" class="totalPrice">Grand Sub Total</td>
				<td align="center" style="background-color: lightgreen;">
				  <input name="grandTotalAmount" type="text" value="" id="totalAmount" class="disabledText" style="width:140px;" disabled />
				</td>
				<td align="center" style="background-color: lightgreen;">
				  <input type="text" class="disabledText" value="${totalProductsInCart}" id="totalQuantities" disabled />
				</td>
			</tr>
		</table>
	</c:if>
	<c:if test="${totalProducts==0}">
		<br />
		<br />
		<table cellpadding="0" class="advertisementsOptionsSpan font">

			<tr>

				<td class="font" style="font-size: 22px; color: red">You don't
					have anything in cart, add items in cart !</td>
				<td class="font">
					<form action="ViewAdsController" method="post">
						<input type="hidden" value="${username}" name="username" /> <input
							type="submit" value="View Advertisement"
							class="font createAdButton" />
					</form>
				</td>
			</tr>
		</table>
	</c:if>
	
	 <c:if test="${totalProductsInCart>0}">
		<form action="CheckoutController" method="get">
			<input type="hidden" name="grandTotal" value="" id="totalAmountHidden" class="disabledText" style="width:140px;" disabled />
			<input type="hidden" id="hiddenUserName" value="${username}" name="buyerUserName" />
			<a href="javascript:submitForm()"><img src="images/checkout.png" title="Checkout and Pay" alt="Submit" height="60" width="200" class="checkOutSpan" /></a>
		</form>
		<form action="ViewAdsController" method="post">
			<input type="hidden" id="hiddenUserName" value="${username}" name="username" />
			
			<input type="image" src="images/add_more.png" title="Add More to Cart!" alt="Submit" class="addToCartSpan" />
		</form>
	</c:if>	
</body>
</html>