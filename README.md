##Simple Market Place - Shopping Application ##

This application enables users to create advertisements for their products and also buy products from the product list. Users need to signup before using this application. 

The technology stack for this project is Core Java, J2EE- Servlets, JSP, HTML, CSS, JavaScript, jQuery & Web Services. For effective application development the app architecture is MVC model based.