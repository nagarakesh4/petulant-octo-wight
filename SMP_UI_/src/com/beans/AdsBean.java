package com.beans;


public class AdsBean {
private String userName;
private String itemName;
private String itemDescription;
private float itemPrice;
private int quantity;
private String sellerInformation;
private String itemId;
private int totalItemsInCart;
private String sellerUserName;
private float totalValueOfItems;
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getItemName() {
	return itemName;
}
public void setItemName(String itemName) {
	this.itemName = itemName;
}
public String getItemDescription() {
	return itemDescription;
}
public void setItemDescription(String itemDescription) {
	this.itemDescription = itemDescription;
}
public float getItemPrice() {
	return itemPrice;
}
public void setItemPrice(float itemPrice) {
	this.itemPrice = itemPrice;
}
public int getQuantity() {
	return quantity;
}
public void setQuantity(int quantity) {
	this.quantity = quantity;
}
public String getSellerInformation() {
	return sellerInformation;
}
public void setSellerInformation(String sellerInformation) {
	this.sellerInformation = sellerInformation;
}
public String getItemId() {
	return itemId;
}
public void setItemId(String itemId) {
	this.itemId = itemId;
}
public void setSellerUserName(String sellerUserName) {
	this.sellerUserName = sellerUserName;
}
public String getSellerUserName() {
	return sellerUserName;
}
public void setTotalValueOfItems(float totalValueOfItems) {
	this.totalValueOfItems = totalValueOfItems;
}
public float getTotalValueOfItems() {
	return totalValueOfItems;
}
public void setTotalItemsInCart(int totalItemsInCart) {
	this.totalItemsInCart = totalItemsInCart;
}
public int getTotalItemsInCart() {
	return totalItemsInCart;
}
}
