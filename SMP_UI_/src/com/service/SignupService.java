package com.service;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Savepoint;

import javax.jws.WebService;

@WebService
public class SignupService {
	public String[] signupUser(String firstName,String lastName,String email,String password) throws SQLException {
		Connection con = null;
		String[] signUpStatus = new String[3];
		final String pattern = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		final String htmlPattern = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			//if(lastName!="" && firstName!="" && email!="" && password!=""){
			if(!"".equals(lastName) && !"".equals(email) && !"".equals(firstName) && !"".equals(password)){
				boolean matcher = email.matches(pattern);
				if(matcher!=false){
					System.out.println("matching firstname " +firstName.matches(htmlPattern));
					if(!firstName.matches(htmlPattern) && !lastName.matches(htmlPattern) && !email.matches(htmlPattern) && !password.matches(htmlPattern) ){
						if (!con.isClosed()) {
							System.out
									.println("Sucessfully Connected to MySql Server using TCP/IP");
							String sqlInsert = "INSERT INTO test.userinfo ( firstname, lastname,emailid,password)VALUES( ?,?,?,?);";
							PreparedStatement preparedStatement = con
									.prepareStatement(sqlInsert);
							preparedStatement.setString(1, firstName);
							preparedStatement.setString(2, lastName);
							preparedStatement.setString(3, email);
							preparedStatement.setString(4, password);
							int newUserInsertionStatus = preparedStatement.executeUpdate();
							System.out.println("user" +newUserInsertionStatus);
							if (newUserInsertionStatus == 1) {
								System.out.println("Signup Successfull!");
								String sqlNewInsert = "INSERT INTO test.logininfo ( username, password)VALUES( ?,?);";
								preparedStatement = con.prepareStatement(sqlNewInsert);
								preparedStatement.setString(1, email);
								System.out.println("before ENCRYPTED PASS WORD IS "+password);
								MessageDigest md = MessageDigest.getInstance("MD5");
								md.update(password.getBytes());
						 
						        byte[] mdbytes = md.digest(); 
								StringBuffer sb = new StringBuffer();
							        for (int i = 0; i < mdbytes.length; i++) {
							          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
							        }
							    preparedStatement.setString(2, sb.toString());
							    System.out.println("ENCRYPTED PASS WORD IS "+sb.toString());
								preparedStatement.setString(2, sb.toString());
								int newLoginInsertionStatus = preparedStatement.executeUpdate();
								Savepoint savepoint = con.setSavepoint();
								System.out.println("login"+newLoginInsertionStatus);
								if (newLoginInsertionStatus == 1) {
									System.out
											.println("new registered values are inserted!");
									signUpStatus[1]="1";
									signUpStatus[2]="Successfully registered";
								} else {
									con.rollback(savepoint);
									System.out.println("here!");
									throw new Exception("$9$Signup failed, try later!!");
								}
							} else {
								System.out.println("or here!");
								throw new Exception("$9$Signup failed, try later!!");
							}
						}
					}else{
						throw new Exception("$2$html tags are not allowed in text boxes");
					}
			}else{
				throw new Exception("$7$emailid entered is not in proper format");
			}
			}else {
				throw new Exception("$4$All the fields must be entered, You cannot leave any field blank");
			}
		} catch (Exception exception) {
			System.out.println(exception);
			if(!String.valueOf(exception).contains("Duplicate entry")){
			signUpStatus = String.valueOf(exception).split("\\$");
			}else{
				signUpStatus[1]="6";
				signUpStatus[2]="Email Id already exists,Enter a new one!";
			}
		} finally {
			con.close();
		}
		System.out.println("the status returned is "+signUpStatus[1]+" for the message "+signUpStatus[2]);
		return signUpStatus;
	}
}
