package com.service;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jws.WebService;

@WebService
public class LoginService {
	public String[] checkDetails(String username, String password) {
		Connection con = null;
		String results[] = new String[10];
		try {
			// create date object
			Date date = new Date();
			// testBean tb = new testBean();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					"MM-dd-yyyy HH:mm:ss");

			// create mysql jdbc driver connection
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");

			// if the connection is successful
			if (!con.isClosed()) {
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");

				// mysql to check if the provided username and password are
				// authenticate.
				String sqlCheck = "select * from logininfo where username=? AND password=?";
				// mysql to record last logged in time and mark current logged in time
				String insertTime = "update logininfo set lastLoggedInTime=currentLoggedInTime,currentLoggedInTime=?,count=count+1 where username=? ";
				// mysql to get the last logged in time of the user
				String getLastLoginTime = "select lastLoggedInTime,count from logininfo where username=?";
				// mysql to retrieve number of items present in cart for this user
				String getNumberOfCartItems = "select sum(quantity) from cartadd where buyerUserName=?";
				// mysql to retrieve last name and first name to display on home page
				String getNameOfUser = "select firstname,lastname from userinfo where emailid=?";
				
				PreparedStatement preparedStatement = con
						.prepareStatement(sqlCheck);

				// get the username and password to fit in the 'sqlCheck'
				preparedStatement.setString(1, username);
				System.out.println("before ENCRYPTED PASS WORD IS "+password);
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
		 
		        byte[] mdbytes = md.digest(); 
				StringBuffer sb = new StringBuffer();
			        for (int i = 0; i < mdbytes.length; i++) {
			          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			        }
			    preparedStatement.setString(2, sb.toString());
			    System.out.println("ENCRYPTED PASS WORD IS "+sb.toString());
				preparedStatement.setString(2, sb.toString());
		
				ResultSet rs = preparedStatement.executeQuery();
				if (!rs.next()) {
					System.out.println("invalid credentials");
					results[0] = "false";
				} else {
					System.out.println("successful credentials");

					preparedStatement = con.prepareStatement(insertTime);
					// get the timestamp and username to fit in the 'insertTime'
					preparedStatement.setTimestamp(1,
							new Timestamp(date.getTime()));
					preparedStatement.setString(2, username);
					int timeStatus = preparedStatement.executeUpdate();
					if (timeStatus == 1) {
						System.out.println("Time inserted successfully");
					}

					// get the username to fit in the 'getLastLoginTime'
					preparedStatement = con.prepareStatement(getLastLoginTime);
					preparedStatement.setString(1, username);
					ResultSet resultSet = preparedStatement.executeQuery();
					if (resultSet.next()) {
						System.out
								.println("Time & count retrieved successfully");
						// retrieve time here
						// tb.setTimestamp(resultSet.getTimestamp(1));
						date = resultSet.getTimestamp(1);
						results[1] = simpleDateFormat.format(date);
						getLastLoginTime = resultSet.getString(2);
						results[2] = getLastLoginTime;
					}
					
					//get the username to fit in the 'getNumberOfCartItems'
					preparedStatement = con.prepareStatement(getNumberOfCartItems);
					preparedStatement.setString(1, username);
					resultSet = preparedStatement.executeQuery();
					if(resultSet.next()){
						results[3]=resultSet.getString(1);
					}
					
					//get the username to fit in the 'getNameOfUser'
					preparedStatement = con.prepareStatement(getNameOfUser);
					preparedStatement.setString(1, username);
					resultSet = preparedStatement.executeQuery();
					if(resultSet.next()){
						results[4]=resultSet.getString(1);
						results[5]=resultSet.getString(2);
					}
					
					results[0] = "true";
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return results;
	}

}
