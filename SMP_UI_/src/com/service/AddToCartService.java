package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

import com.beans.AdsBean;

@WebService
public class AddToCartService {
	public int[] addToCart(AdsBean adsBean) {
		Connection con = null;
		int[] resultsFromCart = new int[3];
		resultsFromCart[0] = 1;
		int updateStatus = 0, addToCartStatus = 0;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			if (!con.isClosed()) {
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");
				// mysql command to check if the cart has already has this item
				String checkDuplicateItem = "Select * from cartadd where buyerUsername=? and itemid=?";

				// mysql command to insert selected item into cart
				String sqlInsertToCart = "INSERT INTO cartadd ( itemid, itemname, itemdescription, itemprice, quantity, sellerinformation, buyerUserName,sellerUserName)"
						+ "VALUES(?,?,?,?,?,?,?,?);";

				// mysql command to retrieve number ofitems in cart based on the
				// quantites
				String numberOfItemsInCart = "Select sum(quantity) from cartadd where buyerUserName=?";

				// update number of quantities ordered if necessary
				String updateQuantities = "update test.cartadd set quantity=quantity+1 where itemid=? and buyerUserName=?";

				// preparing for 'checkDuplicateItem'
				PreparedStatement preparedStatement = con
						.prepareStatement(checkDuplicateItem);
				preparedStatement.setString(1, adsBean.getUserName());
				preparedStatement.setString(2, adsBean.getItemId());
				ResultSet resultSet = preparedStatement.executeQuery();
				// if the same item is added in cart then update the quantity of
				// it.
				if (resultSet.next()) {
					System.out.println("this is a duplicate item");
					
					
					 preparedStatement = con.prepareStatement(updateQuantities);
					 preparedStatement.setString(1, adsBean.getItemId());
					 preparedStatement.setString(2, adsBean.getUserName());
					 updateStatus = preparedStatement.executeUpdate();
					 System.out.println("so updated the quantities the update status " +
					 updateStatus );
					 
					resultsFromCart[0] = 0;
				} else {
					// else insert this item into the cart

					// preparing for 'sqlInsertToCart'
					System.out.println("this is a new item");
					preparedStatement = con.prepareStatement(sqlInsertToCart);
					preparedStatement.setString(1, adsBean.getItemId());
					preparedStatement.setString(2, adsBean.getItemName());
					preparedStatement
							.setString(3, adsBean.getItemDescription());
					preparedStatement.setFloat(4, adsBean.getItemPrice());
					preparedStatement.setInt(5, 1);
					preparedStatement.setString(6,
							adsBean.getSellerInformation());
					preparedStatement.setString(7, adsBean.getUserName());
					preparedStatement.setString(8, adsBean.getSellerUserName());

					addToCartStatus = preparedStatement.executeUpdate();
					System.out.println("so succesfuly inserted into cart "
							+ addToCartStatus);
				}
					// if add into cart is successful or if the update of
					// quantity is successful then retrieve the number of items
					// in the cart
					//if (addToCartStatus == 1) {
						System.out
								.println("Now retrieving the number of items in cart");
						preparedStatement = con
								.prepareStatement(numberOfItemsInCart);
						preparedStatement.setString(1, adsBean.getUserName());
						resultSet = preparedStatement.executeQuery();
						while (resultSet.next()) {
							
							resultsFromCart[1] = resultSet.getInt(1);
						}
						System.out.println(resultsFromCart[1]
								+ " (from service) in cart are present");
				 /*else {
						System.out.println("Insertion in cart failed!!");
					}*/
				
			}
		} catch (Exception exception) {
			System.out.println(exception);
		}
		return resultsFromCart;
	}
}
