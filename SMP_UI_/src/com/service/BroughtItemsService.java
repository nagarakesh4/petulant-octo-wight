package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

import com.beans.AdsBean;
@WebService
public class BroughtItemsService {
	public AdsBean[] retrieveBroughtItems(String username) {

		Connection con = null;
		AdsBean[] adsBeanValuesBrought = new AdsBean[250];
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			int index = 0;

			// mysql to retrieve brought and sold items
			String broughtItems = "select * from checkedoutitems where buyerUserName=?";

			PreparedStatement preparedStatementBrought = con
					.prepareStatement(broughtItems);
			preparedStatementBrought.setString(1, username);
			ResultSet resultSetBrought = preparedStatementBrought
					.executeQuery();
			while (resultSetBrought.next()) {
				AdsBean adsBean = new AdsBean();
				adsBean.setItemId(resultSetBrought.getString(1));
				adsBean.setItemPrice(resultSetBrought.getFloat(2));
				adsBean.setItemName(resultSetBrought.getString(6));
				adsBean.setQuantity(resultSetBrought.getInt(5));
				adsBean.setSellerUserName(resultSetBrought.getString(4));
				adsBeanValuesBrought[index] = adsBean;
				index++;
			}
		} catch (Exception exception) {
			System.out.println(exception);
			exception.printStackTrace();
		}
		return adsBeanValuesBrought;
	}
}
