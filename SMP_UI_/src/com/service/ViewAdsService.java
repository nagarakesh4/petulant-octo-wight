package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

import com.beans.AdsBean;

@WebService
public class ViewAdsService {
	public AdsBean[] retrieveAds(String username) {
		Connection con = null;
		AdsBean[] adsBeanValues = new AdsBean[500];

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			int index = 0;

			// if the connection is successful
			if (!con.isClosed()) {
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");

				// mysql to retrieve all the ads present
				String sqlCheck = "select * from test.adsdetails where quantity>0";

				PreparedStatement preparedStatement = con
						.prepareStatement(sqlCheck);
				ResultSet resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
					System.out.println("processing to display all ads");

					AdsBean adsBean = new AdsBean();
					adsBean.setItemId(resultSet.getString(1));
					adsBean.setItemName(resultSet.getString(2));
					adsBean.setItemDescription(resultSet.getString(3));
					adsBean.setItemPrice(resultSet.getFloat(4));
					adsBean.setQuantity(resultSet.getInt(5));
					adsBean.setSellerInformation(resultSet.getString(6));
					adsBean.setUserName(resultSet.getString(7));

					adsBeanValues[index] = adsBean;
					index++;
				}
			}
		} catch (Exception exception) {
			System.out.println("problems in displaying ads");
			exception.printStackTrace();
		}
		return adsBeanValues;
	}

}
