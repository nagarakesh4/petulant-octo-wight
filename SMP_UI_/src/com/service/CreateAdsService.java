package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.jws.WebService;

@WebService
public class CreateAdsService {
	public String[] createAds(String itemName, String itemDescription,
			String sellerInformation, String itemPrice, String quantity,
			String itemId, String username) throws SQLException {
		Connection con = null;
		String quantityPattern = "^\\d+$";
		String pricePattern = "^(\\d*([.,](?=\\d{3}))?\\d+)+((?!\\2)[.,]\\d\\d)?$";
		String[] adCreate = new String[3];
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			if(!"".equals(itemName) && !"".equals(quantity) && !"".equals(itemId) &&!"".equals(itemDescription) && !"".equals(sellerInformation) && !"".equals(itemPrice)){
			if (!con.isClosed()) {
				if(quantity.matches(quantityPattern)){
					if(itemPrice.matches(pricePattern)){
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");
				float itemPriceFloat = Float.parseFloat(itemPrice);
				int itemQuantityInt = Integer.parseInt(quantity);
				String sqlInsertAds = "INSERT INTO adsdetails ( itemid, itemname, itemdescription, itemprice, quantity, sellerinformation, username)"
						+ "VALUES(?,?,?,?,?,?,?);";
				PreparedStatement preparedStatement = con
						.prepareStatement(sqlInsertAds);
				preparedStatement.setString(1, itemId);
				preparedStatement.setString(2, itemName);
				preparedStatement.setString(3, itemDescription);
				preparedStatement.setFloat(4, itemPriceFloat);
				preparedStatement.setInt(5, itemQuantityInt);
				preparedStatement.setString(6, sellerInformation);
				preparedStatement.setString(7, username);
				int newAdInsertionStatus = preparedStatement.executeUpdate();
				if (newAdInsertionStatus == 1) {
					System.out.println("Ad insertion Successfull!");
					adCreate[1]="1";
					adCreate[2]="Succesfully inserted ad";						
				} else {
					System.out.println("Ad insertion failed!!");
					throw new Exception("$9$Ad Creation failed, try later!!");
				}
			}else{
				adCreate[1]="4";
				adCreate[2]="Only Number is allowerd for Item Price!";
			}
				}else{
				adCreate[1]="4";
				adCreate[2]="Only Integers are allowerd for Quantity!";
			}
			}
		}else{
			System.out.println("All the fields must be entered, You cannot leave any field blank");
			throw new Exception("$4$All the fields must be entered, You cannot leave any field blank");
		}
		} catch (Exception exception) {
			if(!String.valueOf(exception).contains("Duplicate entry")){
				adCreate = String.valueOf(exception).split("\\$");
				}else{
					adCreate[1]="6";
					adCreate[2]="Item Id "+itemId+" already exists,Enter a new one!";
				}
		}
		return adCreate;
	}

}
